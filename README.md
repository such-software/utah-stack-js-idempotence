# Idempotence demo

This is poorly hacked together, but it shows that idempotence doesn't need to be difficult.

It's a node project, using v18.

```
docker-compose rm -sf && docker-compose up
npm install
npm start
```

There are a couple of tests:

```
npm test
```

It's an API server that has two whole endpoints, both off the root.  You can `GET` the root to get a list of "transactions"

You can `PUT` to the root with a JSON body with attributes: `transactionId`, a UUID, and `amount`, a float.  Yeah, we're using floats for money.  Don't do that in the real world.

Example:

```json
{
  "transactionId": "7e9bf577-ca70-48ec-b5b2-ff185041a293",
  "amount": 42
}
```

It returns back an object with the same `transactionId`:

```json
{
  "transactionId": "7e9bf577-ca70-48ec-b5b2-ff185041a293"
}
```

Hot stuff 🔥.

If you go through the code `script/start` is the starting point.  `lib/config.mjs` is a pretty central file.

You'll also see it's a split betweed `.mjs` and `.js` files.  I don't like `import`, but a dependency made it so that I had to convert some of them.

[Slides from the presentation](https://docs.google.com/presentation/d/1EIRAIvQ7ULwqulI-MD3Jlrth3TJ7txZXiYvHu2pWuJQ/edit?usp=sharing)

I'm extremely unlikely to maintain this.  You can use it at your own risk.  MIT license.
