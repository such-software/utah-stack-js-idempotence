exports.up = function(knex) {
  return knex.schema
    .createTable('transfer', table => {
      table.uuid('transfer_id', { primaryKey: true })
      table.float('amount')
      table.timestamps(true, true)
    })
}

exports.down = function(knex) {}
