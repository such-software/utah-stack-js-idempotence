const test = require('blue-tape')
const uuid = require('uuid').v4

const Transfer = require('../../../lib/app/dashboard/actions/transfer')

;(async function () {
  const { db } = await import('../../automated-init.mjs')

  test('Not previously done', t => {
    const transferId = uuid()
  
    const transfer = Transfer({ db })
    const amount = 42

    return transfer(transferId, amount)
      .then(() =>
        db.then(client => client('transfer').where({ transfer_id: transferId}))
      )
      .then(([transfer]) => {
        t.equal(transfer.transfer_id, transferId)
      })
  })
})()
