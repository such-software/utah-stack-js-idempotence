const test = require('blue-tape')
const uuid = require('uuid').v4

const Transfer = require('../../../lib/app/dashboard/actions/transfer')

;(async function () {
  const { db } = await import('../../automated-init.mjs')

  test('Sending it twice', t => {
    const transferId = uuid()
  
    const transfer = Transfer({ db })
    const amount = 42

    return transfer(transferId, amount)
      then(() => transfer(transferId, amount))
      .then(() =>
        db.then(client => client('transfer').where({ transfer_id: transferId}))
      )
      .then(rows => {
        t.equal(rows.length, 1)
      })
  })
})()
