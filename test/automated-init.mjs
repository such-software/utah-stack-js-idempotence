import Bluebird from 'bluebird'
import test from 'blue-tape'

import { app, config } from '../lib/index.mjs'

test.onFinish(() => {
  bootstrappedDb.then(client => client.destroy())
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

function reset () {
  const tablesToWipe = []

  return Bluebird.each(tablesToWipe, table =>
    config.db.then(client => client(table).del())
  )
}

const bootstrappedDb = config.db

export {
  app,
  bootstrappedDb as db,
  reset
}
