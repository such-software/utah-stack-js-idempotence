import 'camelcase-keys'

function build ({ db }) {
  return function loadCurrentUser (req, res, next) {
    return db
      .then(client =>
        client('dashboard')
          .whereNull('forgotten_at')
          .limit(1)
          .then(camelCaseKeys)
          .then(rows => rows[0])
      )
      .then(currentUser => {
        req.context.currentUser = currentUser

        next()
      })
  }
}

export default build
