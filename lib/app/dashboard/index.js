const bodyParser = require('body-parser')
const express = require('express')
const uuid = require('uuid').v4

const Transfer = require("./actions/transfer")

function createHandlers ({ db, transfer }) {
  function index (req, res) {
    return db.then(client => client('transfer'))
      .then(transfers => res.json({
          transfers
       }))
  }

  function put (req, res) {
    const transferId = req.body.transferId

    return transfer(req.body.transferId, req.body.amount)
      .then(() => res.status(201).json({ transferId }))
  }

  return {
    index,
    put
  }
}

function build ({ db }) {
  const transfer = Transfer({ db })
  const handlers = createHandlers({ db, transfer })

  const router = express.Router()

  router
    .route('/')
    .get(handlers.index)
    .put(handlers.put)

  return { handlers, router }
}

module.exports = build
