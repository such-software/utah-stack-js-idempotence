const uuid = require('uuid').v4

function build ({ db }) {
  return async function transfer (transferId, amount) {
    return db
      .then(client => client('transfer')
        .insert({ transfer_id: transferId, amount })
        .onConflict('transfer_id')
        .ignore()
      )
  }
}

module.exports = build
