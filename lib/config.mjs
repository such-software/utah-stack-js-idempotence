/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

import DashboardApp from './app/dashboard/index.js'
import KnexClient from './knex-client.js'
import clock from './clock.js'


function createConfig ({ env }) {
  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })
  const dashboardApp = DashboardApp({ db: knexClient })


  return {
    env,
    db: knexClient,
    dashboardApp
  }
}

export default createConfig
