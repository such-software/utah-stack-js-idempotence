import ExpressApp from './app/express/index.js'
import Config from './config.mjs'
import env from './env.js'

const config = Config({ env })
const app = ExpressApp({ config, env })

function start () {
  app.listen(env.port, signalAppStart)
}

function signalAppStart () {
  console.log(`${env.appName} started`)
  console.table([['Port', env.port], ['Environment', env.env]])
}

export {
  app,
  config,
  start,
}
